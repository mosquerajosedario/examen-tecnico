CREATE DATABASE db_examen_tecnico;

INSERT INTO `db_examen_tecnico`.`carts`
(`id`,
`total`)
VALUES
(0,
0);


INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(1,
"Campera Adidas",
5000,
0);

INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(2,
"Campera Nike",
3000,
0);

INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(3,
"Zapatillas Nike",
7000,
0);

INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(4,
"Zapatillas Adidas",
8000,
0);

INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(5,
"Bermuda Adidas",
2000,
0);

INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(6,
"Bermuda Nike",
2000,
0);

INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(7,
"Gorra Puma",
1000,
0);

INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(8,
"Pantalon Puma",
4000,
0);

INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(9,
"Musculosa Puma",
2000,
0);

INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(10,
"Remera LaCoste",
3000,
0);

INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(11,
"Pantalon LaCoste",
3000,
0);

INSERT INTO `db_examen_tecnico`.`items`
(`id`,
`name`,
`price`,
`cart_id`)
VALUES
(12,
"Pantalon Sara",
3000,
0);

INSERT INTO `db_examen_tecnico`.`carts`
(`id`,
`total`)
VALUES
(0,
0);

INSERT INTO `db_examen_tecnico`.`users`
(`id`,
`user_name`,
`cart_id`,
`vip`,
`update_vip_status`)
VALUES
(1,
"jomosque_arg",
null,
null,
"2021-07-07 15:00:58.110000");

INSERT INTO `db_examen_tecnico`.`users`
(`id`,
`user_name`,
`cart_id`,
`vip`,
`update_vip_status`)
VALUES
(2,
"lrodriguez_org",
null,
null,
"2021-07-07 15:00:58.110000");

INSERT INTO `db_examen_tecnico`.`users`
(`id`,
`user_name`,
`cart_id`,
`vip`,
`update_vip_status`)
VALUES
(3,
"fcaminos_arg",
null,
null,
"2021-07-07 15:00:58.110000");

INSERT INTO `db_examen_tecnico`.`users`
(`id`,
`user_name`,
`cart_id`,
`vip`,
`update_vip_status`)
VALUES
(4,
"mfalbo_arg",
null,
null,
"2021-07-07 15:00:58.110000");