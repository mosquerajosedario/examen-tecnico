package com.examentecnico.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examentecnico.app.entity.Pay;
import com.examentecnico.app.entity.User;
import com.examentecnico.app.service.PayService;

@RestController
@RequestMapping("/api/pays")
public class PayController {
	
	@Autowired
	private PayService payService;
	
	// Create
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Pay pay) {
		return ResponseEntity.status(HttpStatus.CREATED).body(payService.save(pay));
	}
	
	//GetAll
	@GetMapping
    public ResponseEntity<Iterable<Pay>> findAll() {
        return ResponseEntity.ok(payService.findAll());
    }
	
	//GetById
	@GetMapping("/{id}")
	public ResponseEntity<?> read (@PathVariable(value = "id") Long payId) {
		Optional<Pay> oPay = payService.findById(payId);
		
		if(!oPay.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(oPay);
	}
	
	//Get total user by range date
	@GetMapping("/total/{dateInit}/{dateEnd}/userId")
	public ResponseEntity<?> getTotalPurchasesByUserId (
			@PathVariable(value = "dateInit") String dateInit,
			@PathVariable(value = "dateEnd") String dateEnd,
			@PathVariable(value = "userId") Long userId) {
		Iterable<Pay> oPay = payService.getTotalToRangeDateByUserId(dateInit, dateEnd, userId);
		
		return ResponseEntity.ok(oPay);
	}
	
}
