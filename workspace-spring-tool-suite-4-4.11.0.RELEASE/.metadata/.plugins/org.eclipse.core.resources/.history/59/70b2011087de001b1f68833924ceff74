package com.examentecnico.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.examentecnico.app.entity.Cart;
import com.examentecnico.app.entity.Item;
import com.examentecnico.app.repository.CartRepository;

@Service
public class CartServiceImpl implements CartService{

	@Autowired
	private CartRepository cartRepository;
	private CartService cartService;
	private ItemService itemService;
	
	@Override
	@Transactional(readOnly = true)
	public Optional<Cart> findById(Long id) {
		
		return cartRepository.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Cart save(Cart cart) {
		
		return cartRepository.save(cart);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		
		cartService.deleteById(id);
	}
	
	public Cart getCart(Long id){
        return cartService.findById(id).orElseThrow();
    }
	
	@Transactional
    public Cart addItemToCart(Long cartId, Long itemId){
        Cart cart = getCart(cartId);
        Item item = itemService.findById(itemId);
        
        cart.addItem(item);
        
        return cart;
    }

    @Transactional
    public Cart removeItemFromCart(Long cartId, Long itemId){
        Cart cart = getCart(cartId);
        Item item = itemService.getItem(itemId);
        cart.removeItem(item);
        return cart;
    }

}
