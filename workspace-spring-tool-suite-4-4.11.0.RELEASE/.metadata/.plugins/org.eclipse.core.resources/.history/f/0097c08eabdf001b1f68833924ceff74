package com.examentecnico.app.controller;

import java.awt.SystemTray;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examentecnico.app.entity.Cart;
import com.examentecnico.app.entity.Item;
import com.examentecnico.app.entity.Pay;
import com.examentecnico.app.entity.User;
import com.examentecnico.app.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	// Create
	@PostMapping
	public ResponseEntity<?> create(@RequestBody User user) {
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user));
	}
	
	//GetAll
	@GetMapping
    public ResponseEntity<Iterable<User>> findAll() {
        return ResponseEntity.ok(userService.findAll());
    }
	
	//GetById
	@GetMapping("/{id}")
	public ResponseEntity<?> read (@PathVariable(value = "id") Long userId) {
		Optional<User> oUser = userService.findById(userId);
		
		if(!oUser.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(oUser);
	}
	
	//Get users vip and no vip by range date
	@GetMapping("/vip/{dateInit}/{dateEnd}/{vip}")
	public ResponseEntity<?> getUsersVip (
			@PathVariable(value = "dateInit") String dateInit,
			@PathVariable(value = "dateEnd") String dateEnd,
			@PathVariable(value = "vip") Boolean vip) {
		Iterable<User> oUser = userService.findUsersVipToRangeDate(dateInit, dateEnd, vip);
		
		return ResponseEntity.ok(oUser);
	}
	
	//add pay
	@PutMapping("add-pay/{userId}")
	public ResponseEntity<?> addVip(
			@RequestBody Set<Pay> pay,
			@PathVariable(value = "userId") Long userId) {
		
		Optional<User> user = userService.findById(userId);
		
		if(!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		user.get().addItem(pay);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user.get()));
	}
	
	// Add vip
	@PutMapping("add-vip/{userId}")
	public ResponseEntity<?> addVip(
			@PathVariable(value = "userId") Long userId,
			@RequestBody String date) throws ParseException {
		
		Optional<User> user = userService.findById(userId);
		
		if(!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		Date update_vip_status=new SimpleDateFormat("dd/MM/yyyy").parse(date);
		
		user.get().setVip(true);
		user.get().setUpdate_vip_status(update_vip_status);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user.get()));
	}
	
	// Remove vip
	@PutMapping("remove-vip/{userId}")
	public ResponseEntity<?> removeVip(
			@PathVariable(value = "userId") Long userId,
			@RequestBody String date) throws ParseException {
		
		Optional<User> user = userService.findById(userId);
		
		if(!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		Date update_vip_status=new SimpleDateFormat("dd/MM/yyyy").parse(date);
		
		user.get().setVip(false);
		user.get().setUpdate_vip_status(update_vip_status);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user.get()));
	}
	
	//create cart
	@PutMapping("add-cart/{userId}")
	public ResponseEntity<?> addCart (
			@PathVariable(value = "userId") Long userId,
			@RequestBody Cart cart) {
		
		Optional<User> user = userService.findById(userId);
		
		if(!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		if(!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		user.get().setCart(cart);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user.get()));
	}
	
	//remove cart
	@PutMapping("remove-cart/{id}")
	public ResponseEntity<?> removeCart (@PathVariable(value = "id") Long userId) {
		
		Optional<User> user = userService.findById(userId);
		
		if(!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		if(!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		user.get().setCart(null);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user.get()));
	}
}
