package com.examentecnico.app.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examentecnico.app.entity.Cart;
import com.examentecnico.app.entity.Item;
import com.examentecnico.app.entity.User;
import com.examentecnico.app.service.CartService;

@RestController
@RequestMapping("/api/carts")
public class CartController {

	@Autowired
	private CartService cartService;
	
	// Create
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Cart cart) {
		return ResponseEntity.status(HttpStatus.CREATED).body(cartService.save(cart));
	}
	
	//GetById 
	@GetMapping("/{id}")
	public ResponseEntity<?> read (@PathVariable(value = "id") Long cardId) {
		Optional<Cart> oCart = cartService.findById(cardId);
		
		if(!oCart.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(oCart);
	}
	
	//add items
	@PutMapping("add-items/{cartId}")
	public ResponseEntity<?> addItems (
			@RequestBody Set<Item> items, 
			@PathVariable(value = "cartId") Long cartId ) {
		
		Optional<Cart> cart = cartService.findById(cartId);
		
		if(!cart.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		cart.get().addItem(items);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(cartService.save(cart.get()));
	
	}
	
	//remove items
	@PutMapping("remove-items/{cartId}")
	public ResponseEntity<?> removeItems (
			@RequestBody Item item, 
			@PathVariable(value = "cartId") Long cartId ) {
		
		Optional<Cart> cart = cartService.findById(cartId);
		
		if(!cart.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
//		cart.get().setItem(null);
		
		cart.get().removeItems();
		
		return ResponseEntity.status(HttpStatus.CREATED).body(cartService.save(cart.get()));
	
	}
	
	//Delete cart
	@DeleteMapping("delete/{cartId}")
	public ResponseEntity<?> updateAndDelete (@PathVariable(value = "cartId") Long cartId) {
		
		if(!cartService.findById(cartId).isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		cartService.deleteById(cartId);
		return ResponseEntity.ok().build();
	}
	
}
