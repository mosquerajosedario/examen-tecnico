package com.examentecnico.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examentecnico.app.entity.Cart;
import com.examentecnico.app.service.CartService;

@RestController
@RequestMapping("/api/carts")
public class CartController {

	@Autowired
	private CartService cartService;
	
	//Create cart
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Cart cart) {
		return ResponseEntity.status(HttpStatus.CREATED).body(cartService.save(cart));
	}
	
	//GetById 
	@GetMapping("/{id}")
	public ResponseEntity<?> read (@PathVariable(value = "id") Long cardId) {
		Optional<Cart> oCart = cartService.findById(cardId);
		
		if(!oCart.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(oCart);
	}
	
	//Delete cart
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete (@PathVariable(value = "id") Long cartId) {
		
		if(!cartService.findById(cartId).isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		cartService.deleteById(cartId);
		return ResponseEntity.ok().build();
	}
	
	
}
