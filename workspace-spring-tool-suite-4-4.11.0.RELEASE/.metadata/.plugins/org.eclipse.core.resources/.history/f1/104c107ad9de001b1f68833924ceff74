package com.examentecnico.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examentecnico.app.entity.Cart;
import com.examentecnico.app.entity.User;
import com.examentecnico.app.service.CartService;
import com.examentecnico.app.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {
	
	@Autowired
	private UserService userService;
	private CartService cartService;
	
	// Create
	@PostMapping
	public ResponseEntity<?> create(@RequestBody User user) {
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user));
	}
	
	//GetById
	@GetMapping("/{id}")
	public ResponseEntity<?> read (@PathVariable(value = "id") Long userId) {
		Optional<User> oUser = userService.findById(userId);
		
		if(!oUser.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(oUser);
	}
	
	//create cart
	@PutMapping("add-cart/{userId}/{cartId}")
	public ResponseEntity<?> addCart (
			@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "cartId") Long cartId) {
		
		Optional<User> user = userService.findById(userId);
		
		Cart cart = (Cart) cartService.findById(cartId);
		
		if(!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		user.get().setCart(cart);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user.get()));
	}
	
	//remove cart
	@PutMapping("remove-cart/{id}")
	public ResponseEntity<?> removeCart (@PathVariable(value = "id") Long userId) {
		
		Optional<User> user = userService.findById(userId);
		
		if(!user.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		user.get().setCart(null);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user.get()));
	}
}
