package com.examentecnico.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.examentecnico.app.entity.Cart;
import com.examentecnico.app.repository.CartRepository;

@Service
public class CartServiceImpl implements CartService{

	@Autowired
	private CartRepository cartRepository;
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Cart> findAll() {
		
		return cartRepository.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Optional<Cart> findById(Long id) {
		
		return cartRepository.findById(id);
	}

	@Override
	@Transactional
	public Cart save(Cart cart) {
		return cartRepository.save(cart);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		
		cartRepository.deleteById(id);
	}
	
	public Cart getCart(Long id){
        return cartRepository.findById(id).orElseThrow();
    }

	@Transactional
	public Cart getById(Long id) {
		
		return cartRepository.getById(id);
	}

}
