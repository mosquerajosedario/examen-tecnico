package com.examentecnico.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.examentecnico.app.entity.Pay;

@Repository
public interface PayRepository extends JpaRepository<Pay, Long>{

}
