package com.examentecnico.app.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.examentecnico.app.entity.Pay;

@Service
public interface PayService {
	
	public Pay save(Pay pay);
	
	public Iterable<Pay> findAll();
	
	public Optional<Pay> findById(Long id);
	
	public Iterable<Pay> getTotalToRangeDateByUserId(String dateInit, String dateEnd, Long userId);
}
