package com.examentecnico.app.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "carts")
public class Cart implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5528049877051223373L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique=true)
	private Long id;
	
	@Column(nullable = false)
	private Integer total;
		
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cart_id")
	@Column(nullable = false)
    private Set<Item> items;
	
	//Getters And Setters
	
	public Long getId() {
		return id;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Set<Item> getItem() {
		return items;
	}

	public void setItem(Set<Item> item) {
		this.items = item;
	}
	 
	public void addItem(Collection<? extends Item> cart){
        this.items.addAll(cart);
    }

    public void removeItems(){
        this.items.removeAll(items);
    }
    
}
