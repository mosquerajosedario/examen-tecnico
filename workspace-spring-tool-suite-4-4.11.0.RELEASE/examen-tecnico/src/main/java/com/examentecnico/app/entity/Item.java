package com.examentecnico.app.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.lang.Nullable;

@Entity
@Table(name = "items")
public class Item implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5954955683580418632L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique=true, nullable = false)
	private Long id;
	
	@Column(length = 50, nullable = false)
	private String name;
	
	@Column(nullable = false)
	private Integer price;
	
    @ManyToOne
    @Nullable
    private Cart cart;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "item_id")
	@Nullable
    private List<Pay> pay;

	
	//Getters And Setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}
 
	
	
}
