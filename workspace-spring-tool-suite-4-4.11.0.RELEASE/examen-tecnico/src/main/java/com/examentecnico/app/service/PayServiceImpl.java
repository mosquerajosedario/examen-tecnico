package com.examentecnico.app.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.examentecnico.app.entity.Pay;
import com.examentecnico.app.repository.PayRepository;

@Service
public class PayServiceImpl implements PayService{

	@Autowired
	private PayRepository payRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Iterable<Pay> findAll() {
		
		return payRepository.findAll();
	}

	@Override
	public Pay save(Pay pay) {
		
		return payRepository.save(pay);
	}

	@Override
	public Optional<Pay> findById(Long id) {
		
		return payRepository.findById(id);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Pay> getTotalToRangeDateByUserId(String dateInit, String dateEnd, Long userId) {
		
		String queryStr = "select SUM(total) from db_examen_tecnico.pays "
				+"where date_created between '"+dateInit+ "'and '"+dateEnd+"' AND user_id="+userId+";";
		
//		String queryStr = "select * from db_examen_tecnico.users "
//				+ "where update_vip_status between '"+dateInit+"' and '"+dateEnd+"' AND vip="+vip+";";
        
		try {
        	
        	final List<Pay> list = entityManager.createNativeQuery(queryStr).getResultList();

        	
        	Iterable<Pay> iterable = list;        
        	
        	return iterable;
        	
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
	}

}
