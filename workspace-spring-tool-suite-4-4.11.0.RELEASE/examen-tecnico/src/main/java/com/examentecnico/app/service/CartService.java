package com.examentecnico.app.service;

import java.util.Optional;

import com.examentecnico.app.entity.Cart;

public interface CartService {
	
	public Iterable<Cart> findAll();
	
	public Optional<Cart> findById(Long id);
	
	public Cart getById(Long id);
	
	public Cart save(Cart cart);
	
	public void deleteById(Long id);
	
}
