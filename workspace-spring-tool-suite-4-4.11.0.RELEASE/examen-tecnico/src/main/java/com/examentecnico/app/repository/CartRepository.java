package com.examentecnico.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.examentecnico.app.entity.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long>{

}
