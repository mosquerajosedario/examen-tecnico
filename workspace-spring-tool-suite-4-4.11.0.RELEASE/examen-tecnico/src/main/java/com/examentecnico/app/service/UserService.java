package com.examentecnico.app.service;

import java.util.Optional;

import com.examentecnico.app.entity.User;

public interface UserService {
	
	public Iterable<User> findAll();
	
	public Optional<User> findById(Long id);
	
	public User save(User user);

	public Iterable<User> findUsersVipToRangeDate(String dateInit, String dateEnd, Boolean vip);
	
}
