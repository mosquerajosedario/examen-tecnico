package com.examentecnico.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.examentecnico.app.entity.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long>{

}
