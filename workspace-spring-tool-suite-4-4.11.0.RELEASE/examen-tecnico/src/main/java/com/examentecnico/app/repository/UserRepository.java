package com.examentecnico.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.examentecnico.app.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
