package com.examentecnico.app.controller;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examentecnico.app.entity.Cart;
import com.examentecnico.app.entity.Item;
import com.examentecnico.app.service.CartService;

@RestController
@RequestMapping("/api/carts")
public class CartController {

	@Autowired
	private CartService cartService;
	
	private Integer calculeTotal(Cart cart) {
		Integer total = 0;
		
		for (Item item : cart.getItem()) {
			total = total + item.getPrice();
		}
		
		return total;
	}
	
	// Create
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Cart cart) {
		
		//Method that adds the price of the items and sets the total in cart.total
		Integer total = calculeTotal(cart);
		
		cart.setTotal(total);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(cartService.save(cart));
	}
	
	//GetAll
	@GetMapping
    public ResponseEntity<Iterable<Cart>> findAll() {
        return ResponseEntity.ok(cartService.findAll());
    }
	
	//GetById 
	@GetMapping("/{id}")
	public ResponseEntity<?> read (@PathVariable(value = "id") Long cardId) {
		Optional<Cart> oCart = cartService.findById(cardId);
		
		if(!oCart.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(oCart);
	}
	
	//add items
	@PutMapping("update-items/{cartId}")
	public ResponseEntity<?> updateItems (
			@RequestBody Set<Item> items, 
			@PathVariable(value = "cartId") Long cartId ) {
		
		removeItems(cartId);
		
		Optional<Cart> cart = cartService.findById(cartId);
		
		if(!cart.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		cart.get().addItem(items);
		
		Integer total = calculeTotal(cart.get());
		
		cart.get().setTotal(total);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(cartService.save(cart.get()));
	
	}
	
	//remove items
	@PutMapping("remove-items/{cartId}")
	public ResponseEntity<?> removeItems (
			@PathVariable(value = "cartId") Long cartId ) {
		
		Optional<Cart> cart = cartService.findById(cartId);
		
		if(!cart.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		cart.get().removeItems();
		
		return ResponseEntity.status(HttpStatus.CREATED).body(cartService.save(cart.get()));
	
	}
	
	//Delete cart
	@DeleteMapping("delete/{cartId}")
	public ResponseEntity<?> updateAndDelete (@PathVariable(value = "cartId") Long cartId) {
		
		if(!cartService.findById(cartId).isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		cartService.deleteById(cartId);
		return ResponseEntity.ok().build();
	}
	
}
