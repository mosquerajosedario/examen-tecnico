package com.examentecnico.app.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "pays")
public class Pay implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6814184383620683329L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique=true, nullable = false)
	private Long id;
	
	@Column(name = "date_created", insertable = true)
	@CreationTimestamp
	private Date dateCreated;
	
	@Column(nullable = false)
	private Integer total;
	
	@Column(nullable = false)
	private String descount;
	
	
	//Getters And Setters
	
	public Long getId() {
		return id;
	}
	
	public Date getDate() {
		return dateCreated;
	}

	public void setDate(Date date) {
		this.dateCreated = date;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getDescount() {
		return descount;
	}

	public void setDescount(String descount) {
		this.descount = descount;
	}	
	
}
