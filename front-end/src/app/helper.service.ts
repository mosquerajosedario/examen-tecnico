import {Injectable} from "@angular/core";

@Injectable()
export class HelperService {

    constructor() {}

    calculatePromotableDate(_date: Date) {
        var date = new Date(_date);
        if(date.getDay() === 1)
                return true;
        else if(date.getDay() === 4)
                return true;
        else
                return false;
    }

    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('-');
      }
}