import { iItem } from "src/app/item/Interface/intem";

export interface iCart {
    total: number;
    item: iItem[];
}