import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { iCart } from '../interface/cart';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private httpClient: HttpClient) { }

  async create(cart: iCart) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
    };

    const response: any = await this.httpClient.post<any>(
      `/api/carts`, JSON.stringify(cart), httpOptions).toPromise();

    return response;
  }

  async getCartById(id: number) {
    const response: any = await this.httpClient.get<iCart>(`/api/carts/${id}`).toPromise();
    
    return response;    
  }

  async addCartToUserByUserId(userId: number, cart: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
    };

    const response: any = await this.httpClient.put<any>(
      `/api/users/add-cart/${userId}`, JSON.stringify(cart), httpOptions).toPromise();

    return response;
    
  }
}
