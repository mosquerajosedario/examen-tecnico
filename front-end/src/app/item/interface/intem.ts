export interface iItem {
    name: string;
    price: number;
}