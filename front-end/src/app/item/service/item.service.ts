import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { iItem } from '../Interface/intem';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private httpClient: HttpClient) { }

  async getAllItems() {
    const response: any = await this.httpClient.get<iItem>(`/api/items`).toPromise();
    
    return response;    
  }
}
