import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { iPay } from './interface/pay';

@Injectable({
  providedIn: 'root'
})
export class PayService {

  constructor(private httpClient: HttpClient) { }
  
  async create(pay: iPay) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
    };

    const response: any = await this.httpClient.post<any>(
      `/api/pays`, JSON.stringify(pay), httpOptions).toPromise();

    return response;
  }

  async getAllPays() {
    const response: any = await this.httpClient.get<iPay>(`/api/pays`).toPromise();
    
    return response;    
  }

  async getPayById(id: number) {
    const response: any = await this.httpClient.get<iPay>(`/api/pays/${id}`).toPromise();
    
    return response;    
  }

  async getTotalPayToRangeByUserId(dateInit: string, dateEnd: string, userId: number) {

    const response: any = await this.httpClient.get<iPay>(
      `/api/pays/total/${dateInit}/${dateEnd}/${userId}`).toPromise();
    
    return response;
  }
  
}
