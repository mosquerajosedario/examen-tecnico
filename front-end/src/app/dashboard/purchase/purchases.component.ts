import { Component, OnInit, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { UserService } from '../../user/service/user.service';
import { iUser } from '../../user/interface/user';
import { ItemService } from 'src/app/item/service/item.service';
import { iItem } from 'src/app/item/Interface/intem';
import { HelperService } from 'src/app/helper.service';
import { PayService } from 'src/app/pay/pay.service';
import { CartService } from 'src/app/cart/service/cart.service';
import { iCart } from 'src/app/cart/interface/cart';
import { iPay } from 'src/app/pay/interface/pay';

@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.css']
})
export class PurchasesComponent implements OnInit {

  item: iItem = {
    "name": null,
    "price": null
  };

  cart: iCart = {
      "total": 0,
      "item": []
  };

  user: iUser = {
    "id": null,
    "user_name": null,
    "cart": this.cart,
    "vip": null,
    "update_vip_status": null,
    "pay": null
  };

  pay: iPay = {
    "total": 0,
    "descount": null
  }

  listUsers: iUser[] = [];
  listItems: iItem[] = [];
  listAddItems: iItem[] = [];
  fromDate: Date = null;
  promoDate: Boolean = false;
  promoDescription: String;
  firtDay: string;
  lastDay: string;
  // currentDate: any;

  @ViewChildren("checkboxes") checkboxes: QueryList<ElementRef>;

  constructor(
    private userService: UserService,
    private itemService: ItemService,
    private payService: PayService,
    private cartService: CartService,
    private helperService: HelperService
  ) {
  }

  ngOnInit(): void {
    this.userService.getAllUsers().then(response => {
      this.listUsers = response;
    });

    this.getAllItems(); 
  }

  onUserChange(event) {
    this.userService.getUserById(event.target.value).then(response => {
      this.user = response;
    });
  }

  onDateChange(event) {

    if(this.user.id !== null) {
      this.userService.removeCartUserToUserId(this.user.id).then(user => {
        this.user = user;
      });
    }

    // Set firts day and Last day to date input selected.
    this.setFirtsAndLastMonth(event);

    var date = new Date(event), y = date.getFullYear(), m = date.getMonth();
    // this.currentDate = this._formatDate(date);

    if (this.user.id !== null) {
      this.payService.getTotalPayToRangeByUserId(
        this.firtDay, this.lastDay, this.user.id).then(response => {
          if (response[0] > 10000) {
            this.userService.setVipUserToUserId(this.user.id, this._formatDate(date))
            .then(response => {
              this.user = response;
            });
          } else {
            this.userService.setNoVipUserToUserId(this.user.id, this._formatDate(date))
            .then(response => {
              this.user = response;
            });
          }
        });
    }

  }

  setFirtsAndLastMonth(event) {
    this.promoDate = this.helperService.calculatePromotableDate(event);
    
    var date = new Date(event), y = date.getFullYear(), m = date.getMonth();
    var firstDay = new Date(y, m, 1);
    var lastDay = new Date(y, m + 1, 0);
    
    this.firtDay = this.helperService.formatDate(firstDay);

    this.lastDay = this.helperService.formatDate(lastDay);
  }

  payCart() {
    this.applyDiscount(this.user.vip, this.promoDate, this.user.cart.item.length, this.user.cart.total, this.user.cart.item);
  }

  applyDiscount(vip, promoDate, itemQuantity, total, items) {
    
    this.pay.total = total;

    if (itemQuantity === 4) {

      this.pay.total = this.pay.total - this.percentage(25, total);
      this.pay.descount = 'Descuento general 25% por su compra de 4 productos';

      this.payService.create(this.pay).then(pay => {
        this.userService.addPayToUserByUserId(this.user.id, [pay]).then(user => {

          this.userService.removeCartUserToUserId(this.user.id).then(user => {
            this.user = user;
          });
        
        });
      });

      return;

    } else if (itemQuantity > 10) {

      if (vip) {
        var index = 0;
        var totalVip = 0;

        for (let i = 0; i < items.length; i++) {

          if (items[index].price > items[i].price) {
            index = i;
          }

          i++;

        }

        for (let i = 0; i < items.length; i++) {

          if (index !== i) {
            totalVip = totalVip + items[i].price;
          }
        }

        this.pay.total = totalVip - 500;
        this.pay.descount = 'Usuario VIP con compra mayor a 10 productos. Se le bonifica el producto más barato y se aplica un descuento general de $500';

        this.payService.create(this.pay).then(pay => {
          this.userService.addPayToUserByUserId(this.user.id, [pay]).then(user => {

            this.userService.removeCartUserToUserId(this.user.id).then(user => {
              this.user = user;
            });

          });
        });

        return;

      } else if (promoDate) {
        
        this.pay.total = this.pay.total - 300;
        this.pay.descount = 'Carrito promocionable por fecha especial se aplica un descuento general de $300.';

        this.payService.create(this.pay).then(pay => {
          this.userService.addPayToUserByUserId(this.user.id, [pay]).then(user => {

            this.userService.removeCartUserToUserId(this.user.id).then(user => {
              this.user = user;
            });

          });
        });

        return;
      
      } else {

        this.pay.total = this.pay.total - 100;
        this.pay.descount = 'Carrito común se aplica descuento de $100';

        this.payService.create(this.pay).then(pay => {
          this.userService.addPayToUserByUserId(this.user.id, [pay]).then(user => {
            
            this.userService.removeCartUserToUserId(this.user.id).then(user => {
              this.user = user;
            });

          });
        });

        return;

      }
    
    } else {

      this.pay.total = total;
      this.pay.descount = "Ningún descuento aplicado";

      this.payService.create(this.pay).then(pay => {
        this.userService.addPayToUserByUserId(this.user.id, [pay]).then(user => {
          
          this.userService.removeCartUserToUserId(this.user.id).then(user => {
            this.user = user;
          });

        });
      });

      return
    
    }
  }

  percentage(percent, total) {
    return Number(((percent/ 100) * total).toFixed(2))
  }

  _formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [day, month, year].join('-');
  }
  
  getAllItems() {
    this.itemService.getAllItems().then(items => {

      for (var item of items) {
        this.listItems.push(
          {
            "name": item[1],
            "price": item[2]
          }
        );
      }
    });
  }

  isChecked(event, item, index) {
    if (event.target.checked) {
      this.listAddItems.push(item);
    } else {
      var _index = this.listAddItems.findIndex(x => x.name ===item.name);
      if (_index !== -1)
        this.listAddItems.splice(_index, 1);
    }

  }

  isCheckedRemove(event, item, index) {
     if (event.target.checked) {
       var _index = this.listAddItems.findIndex(x => x.name ===item.name);
    
       this.listAddItems.splice(_index, 1);

    } else {
      this.listAddItems.push(item);
    }
  }

  addOrRemoveItemToCart() {
    var cart: iCart = {
      total: 0,
      item: this.listAddItems
    }

    this.cartService.create(cart).then(cart => {
      this.cartService.addCartToUserByUserId(this.user.id, cart).then(user => {
        this.uncheckAll();
        this.user = user;
      });
    });
  
  }

  uncheckAll() {
    this.checkboxes.forEach((element) => {
      element.nativeElement.checked = false;
    });
  }
}
