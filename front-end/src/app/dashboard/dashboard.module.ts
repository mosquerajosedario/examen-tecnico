import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { DashboardRoutingModule } from './dashboard-routing.module';

/* Components */
import { DashboardComponent } from './dashboard/dashboard.component';
import { PurchasesComponent } from './purchase/purchases.component';
import { PaymentComponent } from './payment/payment.component';

@NgModule({
  declarations: [
    DashboardComponent,
    PurchasesComponent,
    PaymentComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[DashboardComponent],
  providers:[]
})
export class DashboardModule { }
