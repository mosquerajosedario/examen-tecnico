import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Component
import { PurchasesComponent } from './purchase/purchases.component';
import { PaymentComponent } from './payment/payment.component';

const routes: Routes = [
  { path: 'purchases', component: PurchasesComponent },
  { path: 'payment', component: PaymentComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
