import { Component, OnInit } from '@angular/core';
import { iCart } from 'src/app/cart/interface/cart';
import { HelperService } from 'src/app/helper.service';
import { iItem } from 'src/app/item/Interface/intem';
import { iPay } from 'src/app/pay/interface/pay';
import { PayService } from 'src/app/pay/pay.service';
import { iUser } from 'src/app/user/Interface/user';
import { UserService } from 'src/app/user/service/user.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  firtsDay: string;
  lastDay: string;
  
  item: iItem = {
    "name": null,
    "price": null
  };

  cart: iCart = {
      "total": 0,
      "item": []
  };

  user: iUser = {
    "id": null,
    "user_name": null,
    "cart": this.cart,
    "vip": null,
    "update_vip_status": null,
    "pay": null
  };

  pay: iPay = {
    "total": 0,
    "descount": null
  }

  vipStatus: boolean = false;
  listPays: iPay[] = [];
  listUsers: iUser[] = [];
  listUserStatusVip: any[] = [];

  constructor(
    private payService: PayService,
    private userService: UserService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
    this.userService.getAllUsers().then(response => {
      this.listUsers = response;
    });

    this.payService.getAllPays().then(response => {
      this.listPays = response;
    })
    
  }

  onUserChange(event) {
    if(event.target.value === 'true') {
      this.payService.getAllPays().then(response => {
        this.listPays = response;
      });

    } else {
      this.userService.getUserById(event.target.value).then(response => {
        this.user = response;
        this.listPays = this.user.pay;
      });
    }
  }

  onDateChange(event) {

    // Set firts day and Last day to date input selected.
    this.setFirtsAndLastMonth(event);

    if (this.user.id !== null) {
      this.userService.getVipStatusUserToRange(this.firtsDay, this.lastDay, this.vipStatus).then(reponse => {
        
      });
    }
  }

  setFirtsAndLastMonth(event) {
    var date = new Date(event), y = date.getFullYear(), m = date.getMonth();
    var firstDay = new Date(y, m, 1);
    var lastDay = new Date(y, m + 1, 0);

    this.firtsDay = this.helperService.formatDate(firstDay);

    this.lastDay = this.helperService.formatDate(lastDay);
  }

  searchUsers() {

    this.userService.getVipStatusUserToRange(this.firtsDay, this.lastDay, this.vipStatus)
    .then(response => {
      this.listUserStatusVip = response;
    });
  }

}
