import { iCart } from "src/app/cart/interface/cart";

export interface iUser {
    id: number;
    user_name: string;
    cart: iCart;
    vip: boolean;
    update_vip_status: any,
    pay: any
}