import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { iUser } from '../Interface/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  async getAllUsers() {
    const response: any = await this.httpClient.get<iUser>(`/api/users`).toPromise();
    
    return response;    
  }

  async getUserById(id: number) {
    const response: any = await this.httpClient.get<iUser>(`/api/users/${id}`).toPromise();
    
    return response;    
  }

  async getVipStatusUserToRange(dateInit: string, dateEnd: string, vip: boolean) {
    const response: any = await this.httpClient.get<any>(`api/users/vip/${dateInit}/${dateEnd}/${vip}`).toPromise();

    return response;
  }

  async addPayToUserByUserId(userId: number, pay: any) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      }),
    };

    const response: any = await this.httpClient.put<any>(
      `/api/users/add-pay/${userId}`, JSON.stringify(pay), httpOptions).toPromise();

    return response;
    
  }

  async setVipUserToUserId(id: number, date: string) {

    const body = new HttpParams()
 
    const response: any = this.httpClient.put<iUser>(`/api/users/add-vip/${date}/${id}`, body).toPromise();

    return response;
  }

  async setNoVipUserToUserId(id: number, date: string) {

    const body = new HttpParams()
 
    const response: any = this.httpClient.put<iUser>(`/api/users/remove-vip/${date}/${id}`, body).toPromise();

    return response;
  }

  async removeCartUserToUserId(id: number) {

    const body = new HttpParams()
 
    const response: any = this.httpClient.put<iUser>(`/api/users/remove-cart/${id}`, body).toPromise();

    return response;
  }
  
}
